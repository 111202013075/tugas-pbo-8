package cobagui;

import java.awt.EventQueue; 

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class cobagui {

	private JFrame frmFormPengisianBarang;
	private JTextField txt_kd_barang;
	private JTextField txt_nm_brg;
	private JTextField txt_stok_brg;
	private JTextField txt_stok_min;
	
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver"; 
    static final String DB_URL = "jdbc:mysql://127.0.0.1/penjualan"; 
    static final String USER = "root";
    static final String PASS = "";

    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    private JTable table;
    private DefaultTableModel model;
    private JScrollPane scrollpane;
    private JTable table_2;
    

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cobagui window = new cobagui();
					window.frmFormPengisianBarang.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public cobagui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFormPengisianBarang = new JFrame();
		frmFormPengisianBarang.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
			}
		});
		frmFormPengisianBarang.setTitle("Form Pengisian Barang");
		frmFormPengisianBarang.setBounds(100, 100, 892, 675);
		frmFormPengisianBarang.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFormPengisianBarang.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Data Barang");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(38, 26, 195, 42);
		frmFormPengisianBarang.getContentPane().add(lblNewLabel);
		
		JLabel kd_barang = new JLabel("Kode Barang");
		kd_barang.setFont(new Font("Tahoma", Font.PLAIN, 15));
		kd_barang.setBounds(38, 100, 108, 23);
		frmFormPengisianBarang.getContentPane().add(kd_barang);
		
		JLabel nm_brg = new JLabel("Nama Barang");
		nm_brg.setFont(new Font("Tahoma", Font.PLAIN, 15));
		nm_brg.setBounds(38, 137, 108, 23);
		frmFormPengisianBarang.getContentPane().add(nm_brg);
		
		JLabel satuan = new JLabel("Satuan");
		satuan.setFont(new Font("Tahoma", Font.PLAIN, 15));
		satuan.setBounds(38, 178, 108, 23);
		frmFormPengisianBarang.getContentPane().add(satuan);
		
		JLabel stok_brg = new JLabel("Stok Barang");
		stok_brg.setFont(new Font("Tahoma", Font.PLAIN, 15));
		stok_brg.setBounds(38, 221, 108, 23);
		frmFormPengisianBarang.getContentPane().add(stok_brg);
		
		JLabel stok_min = new JLabel("Stok Minimal");
		stok_min.setFont(new Font("Tahoma", Font.PLAIN, 15));
		stok_min.setBounds(38, 273, 108, 18);
		frmFormPengisianBarang.getContentPane().add(stok_min);
		
		txt_kd_barang = new JTextField();
		txt_kd_barang.setBounds(156, 100, 119, 23);
		frmFormPengisianBarang.getContentPane().add(txt_kd_barang);
		txt_kd_barang.setColumns(10);
		
		txt_nm_brg = new JTextField();
		txt_nm_brg.setColumns(10);
		txt_nm_brg.setBounds(156, 137, 119, 23);
		frmFormPengisianBarang.getContentPane().add(txt_nm_brg);
		
		final JComboBox cmbx_satuan = new JComboBox();
		cmbx_satuan.setModel(new DefaultComboBoxModel(new String[] {"Buah", "Bungkus", "Liter", "Lembar", "Kilogram", "Karton"}));
		cmbx_satuan.setBounds(156, 175, 77, 23);
		frmFormPengisianBarang.getContentPane().add(cmbx_satuan);
		
		txt_stok_brg = new JTextField();
		txt_stok_brg.setBounds(156, 221, 57, 23);
		frmFormPengisianBarang.getContentPane().add(txt_stok_brg);
		txt_stok_brg.setColumns(10);
		
		txt_stok_min = new JTextField();
		txt_stok_min.setColumns(10);
		txt_stok_min.setBounds(156, 268, 57, 23);
		frmFormPengisianBarang.getContentPane().add(txt_stok_min);
		
		final JButton btnSimpan = new JButton("SIMPAN");
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String kode = txt_kd_barang.getText();
				String nama = txt_nm_brg.getText();
				Object satuan = cmbx_satuan.getSelectedItem();
				int stok = Integer.parseInt(txt_stok_brg.getText());
				int stokmin = Integer.parseInt(txt_stok_min.getText());
				
				System.out.println("Data yang diinput: ");
				System.out.println("Kode Barang: "+kode);
				System.out.println("Nama Barang: "+nama);
				System.out.println("Satuan: "+satuan);
				System.out.println("Stok Barang: "+stok);
				System.out.println("Stok Minimum: "+stokmin);
			}

			private String txt_stok_min() {
				// TODO Auto-generated method stub
				return null;
			}

			private String txt_stok_brg() {
				// TODO Auto-generated method stub
				return null;
			}
		});
		btnSimpan.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSimpan.setBounds(26, 362, 94, 33);
		frmFormPengisianBarang.getContentPane().add(btnSimpan);
		
		JButton btnReset = new JButton("RESET");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetForm();
			}
		});
		btnReset.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnReset.setBounds(211, 431, 94, 33);
		frmFormPengisianBarang.getContentPane().add(btnReset);
		
		JButton btnHapus = new JButton("HAPUS");
		btnHapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Yakin ingin hapus?");
				if(response==0) {
					if(table.getSelectedRow() >=0) {
						hapusData(txt_kd_barang.getText());
					}
				}else {
						JOptionPane.showMessageDialog(null,  "Hapus data batal");
					}
			}

			private void editData() {
				JComboBox boxSatuan = new JComboBos();
				String kode = txt_kd_barang.getText();
				String nama = txt_nm_brg.getText();
				Object satuan = boxSatuan.getSelectedItem();
				int stok = Integer.parseInt(txt_stok_brg.getText());
				int stokmin = Integer.parseInt(txt_stok_min.getText());
				
				try {
					Class.forName(JDBC_DRIVER);
					conn = DriverManager.getConnection(DB_URL,USER,PASS);
					stmt = conn.createStatement();
					
					PreparedStatement ps = conn.prepareStatement("UPDATE barang SET nm_brg=?,satuan=?,stok_brg=?,stok_min=? WHERE kd_barang=?");
					ps.setString(1, nama);
					ps.setObject(2, satuan);
					ps.setInt(3, stok);
					ps.setInt(4, stokmin);
					ps.setString(5, kode);
					
					ps.execute();
					
					JOptionPane.showConfirmDialog(null, "Data berhasil diUpdate");
					stmt.close();
					conn.close();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				showData();
				resetForm();
			}
			
			private void hapusData(String kode) {
				try {
					Class.forName(JDBC_DRIVER);
					conn = DriverManager.getConnection(DB_URL,USER,PASS);
					stmt = conn.createStatement();
					
					PreparedStatement ps = conn.prepareStatement("SELECT * FROM barang WHERE kd_barang=?");
					ps.setString(1, kode);
					
					ps.execute();
					
					stmt.close();
					conn.close();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				showData();
				resetForm();
			}
		});
		btnHapus.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnHapus.setBounds(77, 431, 94, 33);
		frmFormPengisianBarang.getContentPane().add(btnHapus);
		
		JButton btnInsert = new JButton("INSERT");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInsert.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnInsert.setBounds(141, 362, 94, 33);
		frmFormPengisianBarang.getContentPane().add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(435, 67, 413, 539);
		frmFormPengisianBarang.getContentPane().add(scrollPane);
		
		table_2 = new JTable();
		table_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String kode = table.getValueAt(table.getSelectedRow(),0).toString();
				getData(kode);
				btnSimpan.setEnabled(false);
				
			}

			private void getData(String kode) {
				JComboBox boxSatuan = new JComboBox();
				
				try {
					Class.forName(JDBC_DRIVER);
					conn = DriverManager.getConnection(DB_URL,USER,PASS);
					stmt = conn.createStatement();
					
					PreparedStatement ps = conn.prepareStatement("SELECT * FROM barang WHERE kd_barang=?");
					ps.setString(1, kode);
					
					rs = ps.executeQuery();
					rs.next();
					
					txt_kd_barang.setText(rs.getString("kd_barang"));
					txt_nm_brg.setText(rs.getString("nm_brg"));
					boxSatuan.setSelectedItem(rs.getString("satuan"));
					txt_stok_brg.setText(rs.getString("stok_brg"));
					txt_stok_min.setText(rs.getString("stok_min"));
					
					stmt.close();
					conn.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
			}
		});
		scrollPane.setViewportView(table_2);
		
		JButton btnEdit = new JButton("EDIT");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editData();
			}
		});
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnEdit.setBounds(258, 362, 94, 33);
		frmFormPengisianBarang.getContentPane().add(btnEdit);
	}
	
	public void insert(String kode, String nama, Object satuan, int stok, int stokmin)
	{
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			stmt = conn.createStatement();
			
			String sql = "INSERT INTO barang (kd_barang,nm_brg,satuan,stok_brg,stok_min) VALUES (?,?,?,?,?,)";
			PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, kode);
            ps.setString(2, nama);
            ps.setObject(3, satuan);
            ps.setInt(4, stok);
            ps.setInt(5, stokmin);

            ps.execute();

            stmt.close();
            conn.close(); 
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		showData();
	}
	
	private void showData() {
		model = new DefaultTableModel();
		
		model.addColumn("Kode Barang");
		model.addColumn("Nama Barang");
		model.addColumn("Satuan");
		model.addColumn("Stok Barang");
		model.addColumn("Stok Minimal");
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM barang");
			while(rs.next()) {
				model.addRow(new Object[] {
						rs.getString("kd_barang"),
						rs.getString("nm_brg"),
						rs.getString("satuan"),
						rs.getString("stok_brg"),
						rs.getString("stok_min"),
				});
			}
			
			stmt.close();
			conn.close();
			
			table.setModel(model);
			table.setAutoResizeMode(0);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void resetForm() {
		txt_kd_barang.setText("");
		txt_nm_brg.setText("");
		txt_stok_brg.setText("");
		txt_stok_min.setText("");
	}
}
